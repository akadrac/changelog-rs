# **Version 2.7.1** (2021-03-10)  

## Bug Fix  

* git commit failing to add files in single project repo (#afa7b484e)  

* disable tagging if commit fails (#af6a0cd7f)  

* committing fails for multi-project repo (#e9eddde14)  

  In single project repos, it was safe to assume that the last commit on the commit list  
  was the tip of the branch.  
  Due to the filtering of irrelevant commits when versioning separate projects,  
  this was no longer the case, causing failures when trying to commit.  

* truncate files to message length before writing (#238d843cd)  

* parse assemblyinfo versions with four values (#531473951)

# **Version 2.7.0** (2021-03-10)  

## Feature  

* add support for AssemblyInfo.cs files (#6f9079b8e)  

* add support for multi-project repositories (#7eec917a7)

# **Version 2.6.1** (2021-01-21)  

## Refactor  

* improve displaying of version info (#dec9b3d16)  

## Performance  

* misc improvements (#38d649b6c)

# **Version 2.6.0** (2021-01-14)  

## Feature  

* support updating version in Cargo projects (#54180b0f2)

# **Version 2.5.1** (2021-01-12)  

## Bug Fix  

* commit CMakeLists.txt if it exists (#6994249ed)  

## Chore  

* update dependencies (#96a2dee75)

# **Version 2.5.0** (2020-11-11)  

## Feature  

* add support for versioning cmake projects (#52a381cd0)

# **Version 2.4.0** (2020-01-17)  

## Feature  

* add support for "build" type (#0104d5118)  

## Refactor  

* allow for greater testability (#7a834789a)  

* fix clippy warnings (#a321337e1)

# **Version 2.3.0** (2020-01-07)  

## Feature  

* support for muliple entries in a single commit (#2839e4f0f)

# **Version 2.2.0** (2019-11-25)  

## Feature  

* support for auto formatting issue numbers (#65029aee2)  

* scope support (#8327e9bd8)  

* yaml config support (#c4d0e2716)  

## Performance  

* remove slow to_string calls (#7434fe6fd)

# **Version 2.1.1** (2019-11-08)  

## Bug Fix  

* allow tagging after git commit failure (#4ac453221)  

* pre-release string and number in wrong order (#a7dc67115)  

## Refactor  

* allow for integration tests (#60e9374d6)

# **Version 2.1.0** (2019-11-04)  

## Feature  

* support pushing changelog and tags to git (#3a033c303)  

  closes: #1

# **Version 2.0.0** (2019-10-31)  

## Initial release

